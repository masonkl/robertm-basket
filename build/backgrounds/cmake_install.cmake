# Install script for directory: /home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/apps/basket/backgrounds/abstract.png;/usr/local/share/apps/basket/backgrounds/alien-artifact.png;/usr/local/share/apps/basket/backgrounds/alien-artifact.png.config;/usr/local/share/apps/basket/backgrounds/basket-title.png;/usr/local/share/apps/basket/backgrounds/green-curves.png;/usr/local/share/apps/basket/backgrounds/light.png;/usr/local/share/apps/basket/backgrounds/painting.png;/usr/local/share/apps/basket/backgrounds/pens.png;/usr/local/share/apps/basket/backgrounds/pins.png;/usr/local/share/apps/basket/backgrounds/rainbow-balls.png;/usr/local/share/apps/basket/backgrounds/rounds-line.png;/usr/local/share/apps/basket/backgrounds/strings.png;/usr/local/share/apps/basket/backgrounds/todo.png;/usr/local/share/apps/basket/backgrounds/working.png")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/apps/basket/backgrounds" TYPE FILE FILES
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/abstract.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/alien-artifact.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/alien-artifact.png.config"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/basket-title.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/green-curves.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/light.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/painting.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/pens.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/pins.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/rainbow-balls.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/rounds-line.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/strings.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/todo.png"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/backgrounds/working.png"
    )
endif()


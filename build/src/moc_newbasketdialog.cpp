/****************************************************************************
** Meta object code from reading C++ file 'newbasketdialog.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/newbasketdialog.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'newbasketdialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_SingleSelectionKIconView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      30,   26,   25,   25, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SingleSelectionKIconView[] = {
    "SingleSelectionKIconView\0\0cur\0"
    "slotSelectionChanged(QListWidgetItem*)\0"
};

void SingleSelectionKIconView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SingleSelectionKIconView *_t = static_cast<SingleSelectionKIconView *>(_o);
        switch (_id) {
        case 0: _t->slotSelectionChanged((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SingleSelectionKIconView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SingleSelectionKIconView::staticMetaObject = {
    { &QListWidget::staticMetaObject, qt_meta_stringdata_SingleSelectionKIconView,
      qt_meta_data_SingleSelectionKIconView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SingleSelectionKIconView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SingleSelectionKIconView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SingleSelectionKIconView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SingleSelectionKIconView))
        return static_cast<void*>(const_cast< SingleSelectionKIconView*>(this));
    return QListWidget::qt_metacast(_clname);
}

int SingleSelectionKIconView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QListWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_NewBasketDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x09,
      26,   16,   16,   16, 0x09,
      42,   16,   16,   16, 0x09,
      68,   60,   16,   16, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_NewBasketDialog[] = {
    "NewBasketDialog\0\0slotOk()\0returnPressed()\0"
    "manageTemplates()\0newName\0"
    "nameChanged(QString)\0"
};

void NewBasketDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        NewBasketDialog *_t = static_cast<NewBasketDialog *>(_o);
        switch (_id) {
        case 0: _t->slotOk(); break;
        case 1: _t->returnPressed(); break;
        case 2: _t->manageTemplates(); break;
        case 3: _t->nameChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData NewBasketDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject NewBasketDialog::staticMetaObject = {
    { &KDialog::staticMetaObject, qt_meta_stringdata_NewBasketDialog,
      qt_meta_data_NewBasketDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &NewBasketDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *NewBasketDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *NewBasketDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NewBasketDialog))
        return static_cast<void*>(const_cast< NewBasketDialog*>(this));
    return KDialog::qt_metacast(_clname);
}

int NewBasketDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

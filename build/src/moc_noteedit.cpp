/****************************************************************************
** Meta object code from reading C++ file 'noteedit.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/noteedit.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'noteedit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_NoteEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x05,
      28,   11,   11,   11, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_NoteEditor[] = {
    "NoteEditor\0\0askValidation()\0"
    "mouseEnteredEditorWidget()\0"
};

void NoteEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        NoteEditor *_t = static_cast<NoteEditor *>(_o);
        switch (_id) {
        case 0: _t->askValidation(); break;
        case 1: _t->mouseEnteredEditorWidget(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData NoteEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject NoteEditor::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_NoteEditor,
      qt_meta_data_NoteEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &NoteEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *NoteEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *NoteEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NoteEditor))
        return static_cast<void*>(const_cast< NoteEditor*>(this));
    return QObject::qt_metacast(_clname);
}

int NoteEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void NoteEditor::askValidation()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void NoteEditor::mouseEnteredEditorWidget()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
static const uint qt_meta_data_TextEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_TextEditor[] = {
    "TextEditor\0"
};

void TextEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TextEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TextEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_TextEditor,
      qt_meta_data_TextEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TextEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TextEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TextEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TextEditor))
        return static_cast<void*>(const_cast< TextEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int TextEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_HtmlEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x0a,
      36,   11,   11,   11, 0x0a,
      61,   54,   11,   11, 0x0a,
     106,   96,   11,   11, 0x09,
     120,   11,   11,   11, 0x09,
     130,   11,   11,   11, 0x09,
     144,   11,   11,   11, 0x09,
     155,   11,   11,   11, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_HtmlEditor[] = {
    "HtmlEditor\0\0cursorPositionChanged()\0"
    "editTextChanged()\0format\0"
    "charFormatChanged(QTextCharFormat)\0"
    "isChecked\0setBold(bool)\0setLeft()\0"
    "setCentered()\0setRight()\0setBlock()\0"
};

void HtmlEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HtmlEditor *_t = static_cast<HtmlEditor *>(_o);
        switch (_id) {
        case 0: _t->cursorPositionChanged(); break;
        case 1: _t->editTextChanged(); break;
        case 2: _t->charFormatChanged((*reinterpret_cast< const QTextCharFormat(*)>(_a[1]))); break;
        case 3: _t->setBold((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setLeft(); break;
        case 5: _t->setCentered(); break;
        case 6: _t->setRight(); break;
        case 7: _t->setBlock(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData HtmlEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HtmlEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_HtmlEditor,
      qt_meta_data_HtmlEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HtmlEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HtmlEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HtmlEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HtmlEditor))
        return static_cast<void*>(const_cast< HtmlEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int HtmlEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    }
    return _id;
}
static const uint qt_meta_data_ImageEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_ImageEditor[] = {
    "ImageEditor\0"
};

void ImageEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ImageEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ImageEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_ImageEditor,
      qt_meta_data_ImageEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ImageEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ImageEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ImageEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ImageEditor))
        return static_cast<void*>(const_cast< ImageEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int ImageEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_AnimationEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_AnimationEditor[] = {
    "AnimationEditor\0"
};

void AnimationEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData AnimationEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AnimationEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_AnimationEditor,
      qt_meta_data_AnimationEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AnimationEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AnimationEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AnimationEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AnimationEditor))
        return static_cast<void*>(const_cast< AnimationEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int AnimationEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_FileEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_FileEditor[] = {
    "FileEditor\0"
};

void FileEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData FileEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FileEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_FileEditor,
      qt_meta_data_FileEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FileEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FileEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FileEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FileEditor))
        return static_cast<void*>(const_cast< FileEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int FileEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_LinkEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_LinkEditor[] = {
    "LinkEditor\0"
};

void LinkEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData LinkEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LinkEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_LinkEditor,
      qt_meta_data_LinkEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LinkEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LinkEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LinkEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LinkEditor))
        return static_cast<void*>(const_cast< LinkEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int LinkEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_CrossReferenceEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_CrossReferenceEditor[] = {
    "CrossReferenceEditor\0"
};

void CrossReferenceEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData CrossReferenceEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CrossReferenceEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_CrossReferenceEditor,
      qt_meta_data_CrossReferenceEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CrossReferenceEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CrossReferenceEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CrossReferenceEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CrossReferenceEditor))
        return static_cast<void*>(const_cast< CrossReferenceEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int CrossReferenceEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_LauncherEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_LauncherEditor[] = {
    "LauncherEditor\0"
};

void LauncherEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData LauncherEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LauncherEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_LauncherEditor,
      qt_meta_data_LauncherEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LauncherEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LauncherEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LauncherEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LauncherEditor))
        return static_cast<void*>(const_cast< LauncherEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int LauncherEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_ColorEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_ColorEditor[] = {
    "ColorEditor\0"
};

void ColorEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ColorEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ColorEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_ColorEditor,
      qt_meta_data_ColorEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ColorEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ColorEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ColorEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ColorEditor))
        return static_cast<void*>(const_cast< ColorEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int ColorEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_UnknownEditor[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_UnknownEditor[] = {
    "UnknownEditor\0"
};

void UnknownEditor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData UnknownEditor::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject UnknownEditor::staticMetaObject = {
    { &NoteEditor::staticMetaObject, qt_meta_stringdata_UnknownEditor,
      qt_meta_data_UnknownEditor, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &UnknownEditor::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *UnknownEditor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *UnknownEditor::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_UnknownEditor))
        return static_cast<void*>(const_cast< UnknownEditor*>(this));
    return NoteEditor::qt_metacast(_clname);
}

int UnknownEditor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = NoteEditor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_DebuggedLineEdit[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_DebuggedLineEdit[] = {
    "DebuggedLineEdit\0"
};

void DebuggedLineEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData DebuggedLineEdit::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject DebuggedLineEdit::staticMetaObject = {
    { &KLineEdit::staticMetaObject, qt_meta_stringdata_DebuggedLineEdit,
      qt_meta_data_DebuggedLineEdit, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &DebuggedLineEdit::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *DebuggedLineEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *DebuggedLineEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_DebuggedLineEdit))
        return static_cast<void*>(const_cast< DebuggedLineEdit*>(this));
    return KLineEdit::qt_metacast(_clname);
}

int DebuggedLineEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KLineEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_LinkEditDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x09,
      25,   15,   15,   15, 0x09,
      45,   15,   15,   15, 0x09,
      69,   15,   15,   15, 0x09,
      92,   15,   15,   15, 0x09,
     105,   15,   15,   15, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_LinkEditDialog[] = {
    "LinkEditDialog\0\0slotOk()\0urlChanged(QString)\0"
    "doNotAutoTitle(QString)\0doNotAutoIcon(QString)\0"
    "guessTitle()\0guessIcon()\0"
};

void LinkEditDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LinkEditDialog *_t = static_cast<LinkEditDialog *>(_o);
        switch (_id) {
        case 0: _t->slotOk(); break;
        case 1: _t->urlChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->doNotAutoTitle((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->doNotAutoIcon((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->guessTitle(); break;
        case 5: _t->guessIcon(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData LinkEditDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LinkEditDialog::staticMetaObject = {
    { &KDialog::staticMetaObject, qt_meta_stringdata_LinkEditDialog,
      qt_meta_data_LinkEditDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LinkEditDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LinkEditDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LinkEditDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LinkEditDialog))
        return static_cast<void*>(const_cast< LinkEditDialog*>(this));
    return KDialog::qt_metacast(_clname);
}

int LinkEditDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    }
    return _id;
}
static const uint qt_meta_data_CrossReferenceEditDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      26,   25,   25,   25, 0x09,
      41,   35,   25,   25, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_CrossReferenceEditDialog[] = {
    "CrossReferenceEditDialog\0\0slotOk()\0"
    "index\0urlChanged(int)\0"
};

void CrossReferenceEditDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CrossReferenceEditDialog *_t = static_cast<CrossReferenceEditDialog *>(_o);
        switch (_id) {
        case 0: _t->slotOk(); break;
        case 1: _t->urlChanged((*reinterpret_cast< const int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData CrossReferenceEditDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CrossReferenceEditDialog::staticMetaObject = {
    { &KDialog::staticMetaObject, qt_meta_stringdata_CrossReferenceEditDialog,
      qt_meta_data_CrossReferenceEditDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CrossReferenceEditDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CrossReferenceEditDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CrossReferenceEditDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CrossReferenceEditDialog))
        return static_cast<void*>(const_cast< CrossReferenceEditDialog*>(this));
    return KDialog::qt_metacast(_clname);
}

int CrossReferenceEditDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_LauncherEditDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x09,
      29,   19,   19,   19, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_LauncherEditDialog[] = {
    "LauncherEditDialog\0\0slotOk()\0guessIcon()\0"
};

void LauncherEditDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LauncherEditDialog *_t = static_cast<LauncherEditDialog *>(_o);
        switch (_id) {
        case 0: _t->slotOk(); break;
        case 1: _t->guessIcon(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData LauncherEditDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LauncherEditDialog::staticMetaObject = {
    { &KDialog::staticMetaObject, qt_meta_stringdata_LauncherEditDialog,
      qt_meta_data_LauncherEditDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LauncherEditDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LauncherEditDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LauncherEditDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LauncherEditDialog))
        return static_cast<void*>(const_cast< LauncherEditDialog*>(this));
    return KDialog::qt_metacast(_clname);
}

int LauncherEditDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_InlineEditors[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_InlineEditors[] = {
    "InlineEditors\0"
};

void InlineEditors::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData InlineEditors::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject InlineEditors::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_InlineEditors,
      qt_meta_data_InlineEditors, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &InlineEditors::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *InlineEditors::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *InlineEditors::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_InlineEditors))
        return static_cast<void*>(const_cast< InlineEditors*>(this));
    return QObject::qt_metacast(_clname);
}

int InlineEditors::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE

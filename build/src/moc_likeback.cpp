/****************************************************************************
** Meta object code from reading C++ file 'likeback.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/likeback.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'likeback.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LikeBack[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x0a,
      23,    9,    9,    9, 0x0a,
      35,    9,    9,    9, 0x0a,
      99,   60,    9,    9, 0x0a,
     180,  149,    9,    9, 0x2a,
     242,  222,    9,    9, 0x2a,
     281,  276,    9,    9, 0x2a,
     307,    9,    9,    9, 0x2a,
     327,    9,    9,    9, 0x0a,
     345,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_LikeBack[] = {
    "LikeBack\0\0disableBar()\0enableBar()\0"
    "showInformationMessage()\0"
    "type,initialComment,windowPath,context\0"
    "execCommentDialog(Button,QString,QString,QString)\0"
    "type,initialComment,windowPath\0"
    "execCommentDialog(Button,QString,QString)\0"
    "type,initialComment\0"
    "execCommentDialog(Button,QString)\0"
    "type\0execCommentDialog(Button)\0"
    "execCommentDialog()\0askEmailAddress()\0"
    "execCommentDialogFromHelp()\0"
};

void LikeBack::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LikeBack *_t = static_cast<LikeBack *>(_o);
        switch (_id) {
        case 0: _t->disableBar(); break;
        case 1: _t->enableBar(); break;
        case 2: _t->showInformationMessage(); break;
        case 3: _t->execCommentDialog((*reinterpret_cast< Button(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4]))); break;
        case 4: _t->execCommentDialog((*reinterpret_cast< Button(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const QString(*)>(_a[3]))); break;
        case 5: _t->execCommentDialog((*reinterpret_cast< Button(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        case 6: _t->execCommentDialog((*reinterpret_cast< Button(*)>(_a[1]))); break;
        case 7: _t->execCommentDialog(); break;
        case 8: _t->askEmailAddress(); break;
        case 9: _t->execCommentDialogFromHelp(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData LikeBack::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LikeBack::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LikeBack,
      qt_meta_data_LikeBack, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LikeBack::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LikeBack::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LikeBack::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LikeBack))
        return static_cast<void*>(const_cast< LikeBack*>(this));
    return QObject::qt_metacast(_clname);
}

int LikeBack::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

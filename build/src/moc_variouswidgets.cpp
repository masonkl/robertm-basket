/****************************************************************************
** Meta object code from reading C++ file 'variouswidgets.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/variouswidgets.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'variouswidgets.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RunCommandRequester[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      21,   20,   20,   20, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_RunCommandRequester[] = {
    "RunCommandRequester\0\0slotSelCommand()\0"
};

void RunCommandRequester::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RunCommandRequester *_t = static_cast<RunCommandRequester *>(_o);
        switch (_id) {
        case 0: _t->slotSelCommand(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData RunCommandRequester::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RunCommandRequester::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RunCommandRequester,
      qt_meta_data_RunCommandRequester, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RunCommandRequester::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RunCommandRequester::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RunCommandRequester::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RunCommandRequester))
        return static_cast<void*>(const_cast< RunCommandRequester*>(this));
    return QWidget::qt_metacast(_clname);
}

int RunCommandRequester::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_IconSizeCombo[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_IconSizeCombo[] = {
    "IconSizeCombo\0"
};

void IconSizeCombo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData IconSizeCombo::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject IconSizeCombo::staticMetaObject = {
    { &KComboBox::staticMetaObject, qt_meta_stringdata_IconSizeCombo,
      qt_meta_data_IconSizeCombo, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &IconSizeCombo::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *IconSizeCombo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *IconSizeCombo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_IconSizeCombo))
        return static_cast<void*>(const_cast< IconSizeCombo*>(this));
    return KComboBox::qt_metacast(_clname);
}

int IconSizeCombo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KComboBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_ViewSizeDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_ViewSizeDialog[] = {
    "ViewSizeDialog\0"
};

void ViewSizeDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData ViewSizeDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ViewSizeDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_ViewSizeDialog,
      qt_meta_data_ViewSizeDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ViewSizeDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ViewSizeDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ViewSizeDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ViewSizeDialog))
        return static_cast<void*>(const_cast< ViewSizeDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int ViewSizeDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_HelpLabel[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   11,   10,   10, 0x0a,
      39,   10,   10,   10, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_HelpLabel[] = {
    "HelpLabel\0\0message\0setMessage(QString)\0"
    "display()\0"
};

void HelpLabel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HelpLabel *_t = static_cast<HelpLabel *>(_o);
        switch (_id) {
        case 0: _t->setMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->display(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData HelpLabel::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HelpLabel::staticMetaObject = {
    { &KUrlLabel::staticMetaObject, qt_meta_stringdata_HelpLabel,
      qt_meta_data_HelpLabel, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HelpLabel::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HelpLabel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HelpLabel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HelpLabel))
        return static_cast<void*>(const_cast< HelpLabel*>(this));
    return KUrlLabel::qt_metacast(_clname);
}

int HelpLabel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KUrlLabel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_IconSizeDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x09,
      29,   15,   15,   15, 0x09,
      52,   15,   15,   15, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_IconSizeDialog[] = {
    "IconSizeDialog\0\0slotCancel()\0"
    "slotSelectionChanged()\0choose(QListWidgetItem*)\0"
};

void IconSizeDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        IconSizeDialog *_t = static_cast<IconSizeDialog *>(_o);
        switch (_id) {
        case 0: _t->slotCancel(); break;
        case 1: _t->slotSelectionChanged(); break;
        case 2: _t->choose((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData IconSizeDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject IconSizeDialog::staticMetaObject = {
    { &KDialog::staticMetaObject, qt_meta_stringdata_IconSizeDialog,
      qt_meta_data_IconSizeDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &IconSizeDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *IconSizeDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *IconSizeDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_IconSizeDialog))
        return static_cast<void*>(const_cast< IconSizeDialog*>(this));
    return KDialog::qt_metacast(_clname);
}

int IconSizeDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_FontSizeCombo[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      20,   15,   14,   14, 0x05,
      39,   14,   14,   14, 0x05,
      55,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      77,   72,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_FontSizeCombo[] = {
    "FontSizeCombo\0\0size\0sizeChanged(qreal)\0"
    "escapePressed()\0returnPressed2()\0text\0"
    "textChangedInCombo(QString)\0"
};

void FontSizeCombo::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FontSizeCombo *_t = static_cast<FontSizeCombo *>(_o);
        switch (_id) {
        case 0: _t->sizeChanged((*reinterpret_cast< qreal(*)>(_a[1]))); break;
        case 1: _t->escapePressed(); break;
        case 2: _t->returnPressed2(); break;
        case 3: _t->textChangedInCombo((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FontSizeCombo::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FontSizeCombo::staticMetaObject = {
    { &KComboBox::staticMetaObject, qt_meta_stringdata_FontSizeCombo,
      qt_meta_data_FontSizeCombo, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FontSizeCombo::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FontSizeCombo::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FontSizeCombo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FontSizeCombo))
        return static_cast<void*>(const_cast< FontSizeCombo*>(this));
    return KComboBox::qt_metacast(_clname);
}

int FontSizeCombo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KComboBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void FontSizeCombo::sizeChanged(qreal _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void FontSizeCombo::escapePressed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void FontSizeCombo::returnPressed2()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE

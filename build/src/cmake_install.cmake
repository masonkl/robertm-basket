# Install script for directory: /home/masonkl/CMSC461/SemesterProject/robertm-basket/src

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  foreach(file
      "$ENV{DESTDIR}/usr/local/lib/libbasketcommon.so.4.14.19"
      "$ENV{DESTDIR}/usr/local/lib/libbasketcommon.so.4"
      "$ENV{DESTDIR}/usr/local/lib/libbasketcommon.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "/usr/local/lib")
    endif()
  endforeach()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/lib/libbasketcommon.so.4.14.19;/usr/local/lib/libbasketcommon.so.4;/usr/local/lib/libbasketcommon.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/lib" TYPE SHARED_LIBRARY FILES
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/build/lib/libbasketcommon.so.4.14.19"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/build/lib/libbasketcommon.so.4"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/build/lib/libbasketcommon.so"
    )
  foreach(file
      "$ENV{DESTDIR}/usr/local/lib/libbasketcommon.so.4.14.19"
      "$ENV{DESTDIR}/usr/local/lib/libbasketcommon.so.4"
      "$ENV{DESTDIR}/usr/local/lib/libbasketcommon.so"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHANGE
           FILE "${file}"
           OLD_RPATH "::::::::::::::"
           NEW_RPATH "/usr/local/lib")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}/usr/local/bin/basket" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/bin/basket")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/bin/basket"
         RPATH "/usr/local/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/bin/basket")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/bin" TYPE EXECUTABLE FILES "/home/masonkl/CMSC461/SemesterProject/robertm-basket/build/src/basket")
  if(EXISTS "$ENV{DESTDIR}/usr/local/bin/basket" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/bin/basket")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/usr/local/bin/basket"
         OLD_RPATH "/home/masonkl/CMSC461/SemesterProject/robertm-basket/build/lib:"
         NEW_RPATH "/usr/local/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/bin/basket")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}/usr/local/lib/kde4/kcm_basket.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/kde4/kcm_basket.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/lib/kde4/kcm_basket.so"
         RPATH "/usr/local/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/lib/kde4/kcm_basket.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/lib/kde4" TYPE MODULE FILES "/home/masonkl/CMSC461/SemesterProject/robertm-basket/build/lib/kcm_basket.so")
  if(EXISTS "$ENV{DESTDIR}/usr/local/lib/kde4/kcm_basket.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/kde4/kcm_basket.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/usr/local/lib/kde4/kcm_basket.so"
         OLD_RPATH "/home/masonkl/CMSC461/SemesterProject/robertm-basket/build/lib:"
         NEW_RPATH "/usr/local/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/lib/kde4/kcm_basket.so")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/applications/kde4/basket.desktop")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/applications/kde4" TYPE FILE FILES "/home/masonkl/CMSC461/SemesterProject/robertm-basket/src/basket.desktop")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/kde4/services/basket_config_general.desktop;/usr/local/share/kde4/services/basket_config_baskets.desktop;/usr/local/share/kde4/services/basket_config_new_notes.desktop;/usr/local/share/kde4/services/basket_config_notes_appearance.desktop;/usr/local/share/kde4/services/basket_config_apps.desktop")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/kde4/services" TYPE FILE FILES
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/src/basket_config_general.desktop"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/src/basket_config_baskets.desktop"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/src/basket_config_new_notes.desktop"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/src/basket_config_notes_appearance.desktop"
    "/home/masonkl/CMSC461/SemesterProject/robertm-basket/src/basket_config_apps.desktop"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/apps/basket/basketui.rc")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/apps/basket" TYPE FILE FILES "/home/masonkl/CMSC461/SemesterProject/robertm-basket/src/basketui.rc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  
    set(DESTDIR_VALUE "$ENV{DESTDIR}")
    if (NOT DESTDIR_VALUE)
        file(WRITE "/usr/local/share/icons/hicolor/temp.txt" "update")
        file(REMOVE "/usr/local/share/icons/hicolor/temp.txt")
    endif (NOT DESTDIR_VALUE)
    
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/masonkl/CMSC461/SemesterProject/robertm-basket/build/src/tests/cmake_install.cmake")

endif()


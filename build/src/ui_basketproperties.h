#include <kdialog.h>
#include <klocale.h>

/********************************************************************************
** Form generated from reading UI file 'basketproperties.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BASKETPROPERTIES_H
#define UI_BASKETPROPERTIES_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QFontComboBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "KIconButton"
#include "knuminput.h"
#include "kshortcutwidget.h"

QT_BEGIN_NAMESPACE

class Ui_BasketPropertiesUi
{
public:
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout;
    KIconButton *icon;
    QLineEdit *name;
    QGroupBox *dispositionGroup;
    QGridLayout *gridLayout_3;
    QRadioButton *columnForm;
    QRadioButton *freeForm;
    KIntNumInput *columnCount;
    QSpacerItem *horizontalSpacer;
    QRadioButton *mindMap;
    QCheckBox *pasteFormatted;
    QGroupBox *shortcutGroup;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *shortcutLayout;
    KShortcutWidget *shortcut;
    QVBoxLayout *verticalLayout_6;
    QRadioButton *showBasket;
    QRadioButton *globalButton;
    QRadioButton *switchButton;
    QGroupBox *appearanceGroup;
    QGridLayout *appearanceLayout;
    QComboBox *backgroundImage;
    QLabel *label;
    QLabel *bgColorLbl;
    QLabel *txtColorLbl;
    QLabel *label_2;
    QFontComboBox *defaultFont;

    void setupUi(QWidget *BasketPropertiesUi)
    {
        if (BasketPropertiesUi->objectName().isEmpty())
            BasketPropertiesUi->setObjectName(QString::fromUtf8("BasketPropertiesUi"));
        BasketPropertiesUi->resize(633, 361);
        gridLayout_2 = new QGridLayout(BasketPropertiesUi);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        icon = new KIconButton(BasketPropertiesUi);
        icon->setObjectName(QString::fromUtf8("icon"));

        horizontalLayout->addWidget(icon);

        name = new QLineEdit(BasketPropertiesUi);
        name->setObjectName(QString::fromUtf8("name"));

        horizontalLayout->addWidget(name);


        gridLayout_2->addLayout(horizontalLayout, 0, 0, 1, 2);

        dispositionGroup = new QGroupBox(BasketPropertiesUi);
        dispositionGroup->setObjectName(QString::fromUtf8("dispositionGroup"));
        gridLayout_3 = new QGridLayout(dispositionGroup);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        columnForm = new QRadioButton(dispositionGroup);
        columnForm->setObjectName(QString::fromUtf8("columnForm"));

        gridLayout_3->addWidget(columnForm, 0, 0, 1, 1);

        freeForm = new QRadioButton(dispositionGroup);
        freeForm->setObjectName(QString::fromUtf8("freeForm"));

        gridLayout_3->addWidget(freeForm, 1, 0, 1, 1);

        columnCount = new KIntNumInput(dispositionGroup);
        columnCount->setObjectName(QString::fromUtf8("columnCount"));

        gridLayout_3->addWidget(columnCount, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer, 1, 1, 1, 1);

        mindMap = new QRadioButton(dispositionGroup);
        mindMap->setObjectName(QString::fromUtf8("mindMap"));

        gridLayout_3->addWidget(mindMap, 2, 0, 1, 1);

        pasteFormatted = new QCheckBox(dispositionGroup);
        pasteFormatted->setObjectName(QString::fromUtf8("pasteFormatted"));

        gridLayout_3->addWidget(pasteFormatted, 3, 0, 1, 1);


        gridLayout_2->addWidget(dispositionGroup, 1, 1, 1, 1);

        shortcutGroup = new QGroupBox(BasketPropertiesUi);
        shortcutGroup->setObjectName(QString::fromUtf8("shortcutGroup"));
        verticalLayout_7 = new QVBoxLayout(shortcutGroup);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(4, 4, 4, 4);
        shortcutLayout = new QHBoxLayout();
        shortcutLayout->setObjectName(QString::fromUtf8("shortcutLayout"));
        shortcut = new KShortcutWidget(shortcutGroup);
        shortcut->setObjectName(QString::fromUtf8("shortcut"));

        shortcutLayout->addWidget(shortcut);


        verticalLayout_7->addLayout(shortcutLayout);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        showBasket = new QRadioButton(shortcutGroup);
        showBasket->setObjectName(QString::fromUtf8("showBasket"));

        verticalLayout_6->addWidget(showBasket);

        globalButton = new QRadioButton(shortcutGroup);
        globalButton->setObjectName(QString::fromUtf8("globalButton"));

        verticalLayout_6->addWidget(globalButton);

        switchButton = new QRadioButton(shortcutGroup);
        switchButton->setObjectName(QString::fromUtf8("switchButton"));

        verticalLayout_6->addWidget(switchButton);


        verticalLayout_7->addLayout(verticalLayout_6);


        gridLayout_2->addWidget(shortcutGroup, 3, 0, 1, 2);

        appearanceGroup = new QGroupBox(BasketPropertiesUi);
        appearanceGroup->setObjectName(QString::fromUtf8("appearanceGroup"));
        appearanceLayout = new QGridLayout(appearanceGroup);
        appearanceLayout->setObjectName(QString::fromUtf8("appearanceLayout"));
        appearanceLayout->setContentsMargins(4, 4, 4, 4);
        backgroundImage = new QComboBox(appearanceGroup);
        backgroundImage->setObjectName(QString::fromUtf8("backgroundImage"));
        backgroundImage->setEnabled(true);

        appearanceLayout->addWidget(backgroundImage, 0, 2, 1, 1);

        label = new QLabel(appearanceGroup);
        label->setObjectName(QString::fromUtf8("label"));

        appearanceLayout->addWidget(label, 0, 1, 1, 1);

        bgColorLbl = new QLabel(appearanceGroup);
        bgColorLbl->setObjectName(QString::fromUtf8("bgColorLbl"));

        appearanceLayout->addWidget(bgColorLbl, 1, 1, 1, 1);

        txtColorLbl = new QLabel(appearanceGroup);
        txtColorLbl->setObjectName(QString::fromUtf8("txtColorLbl"));

        appearanceLayout->addWidget(txtColorLbl, 3, 1, 1, 1);

        label_2 = new QLabel(appearanceGroup);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        appearanceLayout->addWidget(label_2, 4, 1, 1, 1);

        defaultFont = new QFontComboBox(appearanceGroup);
        defaultFont->setObjectName(QString::fromUtf8("defaultFont"));

        appearanceLayout->addWidget(defaultFont, 4, 2, 1, 1);


        gridLayout_2->addWidget(appearanceGroup, 1, 0, 1, 1);

#ifndef UI_QT_NO_SHORTCUT
        label->setBuddy(backgroundImage);
#endif // QT_NO_SHORTCUT
        QWidget::setTabOrder(icon, name);
        QWidget::setTabOrder(name, backgroundImage);
        QWidget::setTabOrder(backgroundImage, columnForm);
        QWidget::setTabOrder(columnForm, freeForm);
        QWidget::setTabOrder(freeForm, mindMap);
        QWidget::setTabOrder(mindMap, columnCount);
        QWidget::setTabOrder(columnCount, showBasket);
        QWidget::setTabOrder(showBasket, globalButton);
        QWidget::setTabOrder(globalButton, switchButton);

        retranslateUi(BasketPropertiesUi);

        QMetaObject::connectSlotsByName(BasketPropertiesUi);
    } // setupUi

    void retranslateUi(QWidget *BasketPropertiesUi)
    {
        BasketPropertiesUi->setWindowTitle(tr2i18n("Form", 0));
        dispositionGroup->setTitle(tr2i18n("Disposition", 0));
        columnForm->setText(tr2i18n("Col&umns:", 0));
        freeForm->setText(tr2i18n("&Free-form", 0));
        mindMap->setText(tr2i18n("&Mind map", 0));
        pasteFormatted->setText(tr2i18n("Paste Formatted Text", 0));
        shortcutGroup->setTitle(tr2i18n("Keyboard Shortcut", 0));
        showBasket->setText(tr2i18n("Show &this basket", 0));
        globalButton->setText(tr2i18n("Show this basket (&global shortcut)", 0));
        switchButton->setText(tr2i18n("Switch to this basket (g&lobal shortcut)", 0));
        appearanceGroup->setTitle(tr2i18n("Appearance", 0));
        label->setText(tr2i18n("Bac&kground image:", 0));
        bgColorLbl->setText(tr2i18n("&Background color:", 0));
        txtColorLbl->setText(tr2i18n("&Text color:", 0));
        label_2->setText(tr2i18n("Default Font", 0));
    } // retranslateUi

};

namespace Ui {
    class BasketPropertiesUi: public Ui_BasketPropertiesUi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // BASKETPROPERTIES_H


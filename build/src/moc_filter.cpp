/****************************************************************************
** Meta object code from reading C++ file 'filter.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/filter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'filter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FilterBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      16,   11,   10,   10, 0x05,

 // slots: signature, parameters, type, tag, flags
      38,   10,   10,   10, 0x0a,
      60,   10,   10,   10, 0x0a,
      68,   10,   10,   10, 0x0a,
      83,   10,   10,   10, 0x0a,
     102,   98,   10,   10, 0x0a,
     124,  118,   10,   10, 0x0a,
     154,  144,   10,   10, 0x0a,
     173,   11,   10,   10, 0x0a,
     199,   10,   10,   10, 0x08,
     220,  214,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_FilterBar[] = {
    "FilterBar\0\0data\0newFilter(FilterData)\0"
    "repopulateTagsCombo()\0reset()\0"
    "inAllBaskets()\0setEditFocus()\0tag\0"
    "filterTag(Tag*)\0state\0filterState(State*)\0"
    "filterAll\0setFilterAll(bool)\0"
    "setFilterData(FilterData)\0changeFilter()\0"
    "index\0tagChanged(int)\0"
};

void FilterBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FilterBar *_t = static_cast<FilterBar *>(_o);
        switch (_id) {
        case 0: _t->newFilter((*reinterpret_cast< const FilterData(*)>(_a[1]))); break;
        case 1: _t->repopulateTagsCombo(); break;
        case 2: _t->reset(); break;
        case 3: _t->inAllBaskets(); break;
        case 4: _t->setEditFocus(); break;
        case 5: _t->filterTag((*reinterpret_cast< Tag*(*)>(_a[1]))); break;
        case 6: _t->filterState((*reinterpret_cast< State*(*)>(_a[1]))); break;
        case 7: _t->setFilterAll((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->setFilterData((*reinterpret_cast< const FilterData(*)>(_a[1]))); break;
        case 9: _t->changeFilter(); break;
        case 10: _t->tagChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FilterBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FilterBar::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_FilterBar,
      qt_meta_data_FilterBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FilterBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FilterBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FilterBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FilterBar))
        return static_cast<void*>(const_cast< FilterBar*>(this));
    return QWidget::qt_metacast(_clname);
}

int FilterBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void FilterBar::newFilter(const FilterData & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE

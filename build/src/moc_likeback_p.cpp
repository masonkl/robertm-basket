/****************************************************************************
** Meta object code from reading C++ file 'likeback_p.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/likeback_p.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'likeback_p.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LikeBackBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x0a,
      26,   12,   12,   12, 0x0a,
      38,   12,   12,   12, 0x08,
      49,   12,   12,   12, 0x08,
      63,   12,   12,   12, 0x08,
      80,   12,   12,   12, 0x08,
      93,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_LikeBackBar[] = {
    "LikeBackBar\0\0startTimer()\0stopTimer()\0"
    "autoMove()\0clickedLike()\0clickedDislike()\0"
    "clickedBug()\0clickedFeature()\0"
};

void LikeBackBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LikeBackBar *_t = static_cast<LikeBackBar *>(_o);
        switch (_id) {
        case 0: _t->startTimer(); break;
        case 1: _t->stopTimer(); break;
        case 2: _t->autoMove(); break;
        case 3: _t->clickedLike(); break;
        case 4: _t->clickedDislike(); break;
        case 5: _t->clickedBug(); break;
        case 6: _t->clickedFeature(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData LikeBackBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LikeBackBar::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_LikeBackBar,
      qt_meta_data_LikeBackBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LikeBackBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LikeBackBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LikeBackBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LikeBackBar))
        return static_cast<void*>(const_cast< LikeBackBar*>(this));
    return QWidget::qt_metacast(_clname);
}

int LikeBackBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
static const uint qt_meta_data_LikeBackDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      33,   15,   15,   15, 0x08,
      47,   15,   15,   15, 0x08,
      56,   15,   15,   15, 0x08,
      81,   15,   15,   15, 0x08,
      98,   15,   15,   15, 0x08,
     111,  105,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_LikeBackDialog[] = {
    "LikeBackDialog\0\0ensurePolished()\0"
    "slotDefault()\0slotOk()\0changeButtonBarVisible()\0"
    "commentChanged()\0send()\0reply\0"
    "requestFinished(QNetworkReply*)\0"
};

void LikeBackDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LikeBackDialog *_t = static_cast<LikeBackDialog *>(_o);
        switch (_id) {
        case 0: _t->ensurePolished(); break;
        case 1: _t->slotDefault(); break;
        case 2: _t->slotOk(); break;
        case 3: _t->changeButtonBarVisible(); break;
        case 4: _t->commentChanged(); break;
        case 5: _t->send(); break;
        case 6: _t->requestFinished((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData LikeBackDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LikeBackDialog::staticMetaObject = {
    { &KDialog::staticMetaObject, qt_meta_stringdata_LikeBackDialog,
      qt_meta_data_LikeBackDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LikeBackDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LikeBackDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LikeBackDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LikeBackDialog))
        return static_cast<void*>(const_cast< LikeBackDialog*>(this));
    return KDialog::qt_metacast(_clname);
}

int LikeBackDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'focusedwidgets.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/focusedwidgets.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'focusedwidgets.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FocusedTextEdit[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      17,   16,   16,   16, 0x05,
      33,   16,   16,   16, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_FocusedTextEdit[] = {
    "FocusedTextEdit\0\0escapePressed()\0"
    "mouseEntered()\0"
};

void FocusedTextEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FocusedTextEdit *_t = static_cast<FocusedTextEdit *>(_o);
        switch (_id) {
        case 0: _t->escapePressed(); break;
        case 1: _t->mouseEntered(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData FocusedTextEdit::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FocusedTextEdit::staticMetaObject = {
    { &KTextEdit::staticMetaObject, qt_meta_stringdata_FocusedTextEdit,
      qt_meta_data_FocusedTextEdit, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FocusedTextEdit::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FocusedTextEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FocusedTextEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FocusedTextEdit))
        return static_cast<void*>(const_cast< FocusedTextEdit*>(this));
    return KTextEdit::qt_metacast(_clname);
}

int FocusedTextEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KTextEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void FocusedTextEdit::escapePressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void FocusedTextEdit::mouseEntered()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
static const uint qt_meta_data_FocusWidgetFilter[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x05,
      35,   18,   18,   18, 0x05,
      51,   18,   18,   18, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_FocusWidgetFilter[] = {
    "FocusWidgetFilter\0\0escapePressed()\0"
    "returnPressed()\0mouseEntered()\0"
};

void FocusWidgetFilter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FocusWidgetFilter *_t = static_cast<FocusWidgetFilter *>(_o);
        switch (_id) {
        case 0: _t->escapePressed(); break;
        case 1: _t->returnPressed(); break;
        case 2: _t->mouseEntered(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData FocusWidgetFilter::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FocusWidgetFilter::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FocusWidgetFilter,
      qt_meta_data_FocusWidgetFilter, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FocusWidgetFilter::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FocusWidgetFilter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FocusWidgetFilter::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FocusWidgetFilter))
        return static_cast<void*>(const_cast< FocusWidgetFilter*>(this));
    return QObject::qt_metacast(_clname);
}

int FocusWidgetFilter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}

// SIGNAL 0
void FocusWidgetFilter::escapePressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void FocusWidgetFilter::returnPressed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void FocusWidgetFilter::mouseEntered()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE

/****************************************************************************
** Meta object code from reading C++ file 'notecontent.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/notecontent.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'notecontent.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_AnimationContent[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      18,   17,   17,   17, 0x09,
      33,   17,   17,   17, 0x09,
      48,   17,   17,   17, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_AnimationContent[] = {
    "AnimationContent\0\0movieUpdated()\0"
    "movieResized()\0movieFrameChanged()\0"
};

void AnimationContent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        AnimationContent *_t = static_cast<AnimationContent *>(_o);
        switch (_id) {
        case 0: _t->movieUpdated(); break;
        case 1: _t->movieResized(); break;
        case 2: _t->movieFrameChanged(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData AnimationContent::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject AnimationContent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_AnimationContent,
      qt_meta_data_AnimationContent, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &AnimationContent::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *AnimationContent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *AnimationContent::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_AnimationContent))
        return static_cast<void*>(const_cast< AnimationContent*>(this));
    if (!strcmp(_clname, "NoteContent"))
        return static_cast< NoteContent*>(const_cast< AnimationContent*>(this));
    return QObject::qt_metacast(_clname);
}

int AnimationContent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_FileContent[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      22,   13,   12,   12, 0x09,
      52,   12,   12,   12, 0x09,
      77,   12,   12,   12, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_FileContent[] = {
    "FileContent\0\0,preview\0"
    "newPreview(KFileItem,QPixmap)\0"
    "removePreview(KFileItem)\0"
    "startFetchingUrlPreview()\0"
};

void FileContent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FileContent *_t = static_cast<FileContent *>(_o);
        switch (_id) {
        case 0: _t->newPreview((*reinterpret_cast< const KFileItem(*)>(_a[1])),(*reinterpret_cast< const QPixmap(*)>(_a[2]))); break;
        case 1: _t->removePreview((*reinterpret_cast< const KFileItem(*)>(_a[1]))); break;
        case 2: _t->startFetchingUrlPreview(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FileContent::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FileContent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FileContent,
      qt_meta_data_FileContent, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FileContent::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FileContent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FileContent::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FileContent))
        return static_cast<void*>(const_cast< FileContent*>(this));
    if (!strcmp(_clname, "NoteContent"))
        return static_cast< NoteContent*>(const_cast< FileContent*>(this));
    return QObject::qt_metacast(_clname);
}

int FileContent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_SoundContent[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   14,   13,   13, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_SoundContent[] = {
    "SoundContent\0\0,\0"
    "stateChanged(Phonon::State,Phonon::State)\0"
};

void SoundContent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        SoundContent *_t = static_cast<SoundContent *>(_o);
        switch (_id) {
        case 0: _t->stateChanged((*reinterpret_cast< Phonon::State(*)>(_a[1])),(*reinterpret_cast< Phonon::State(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData SoundContent::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject SoundContent::staticMetaObject = {
    { &FileContent::staticMetaObject, qt_meta_stringdata_SoundContent,
      qt_meta_data_SoundContent, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &SoundContent::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *SoundContent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *SoundContent::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SoundContent))
        return static_cast<void*>(const_cast< SoundContent*>(this));
    return FileContent::qt_metacast(_clname);
}

int SoundContent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = FileContent::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_LinkContent[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x09,
      35,   29,   12,   12, 0x09,
      69,   60,   12,   12, 0x09,
      99,   12,   12,   12, 0x09,
     124,   12,   12,   12, 0x09,

       0        // eod
};

static const char qt_meta_stringdata_LinkContent[] = {
    "LinkContent\0\0httpReadyRead()\0reply\0"
    "httpDone(QNetworkReply*)\0,preview\0"
    "newPreview(KFileItem,QPixmap)\0"
    "removePreview(KFileItem)\0"
    "startFetchingUrlPreview()\0"
};

void LinkContent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        LinkContent *_t = static_cast<LinkContent *>(_o);
        switch (_id) {
        case 0: _t->httpReadyRead(); break;
        case 1: _t->httpDone((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 2: _t->newPreview((*reinterpret_cast< const KFileItem(*)>(_a[1])),(*reinterpret_cast< const QPixmap(*)>(_a[2]))); break;
        case 3: _t->removePreview((*reinterpret_cast< const KFileItem(*)>(_a[1]))); break;
        case 4: _t->startFetchingUrlPreview(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData LinkContent::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject LinkContent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_LinkContent,
      qt_meta_data_LinkContent, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LinkContent::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LinkContent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LinkContent::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LinkContent))
        return static_cast<void*>(const_cast< LinkContent*>(this));
    if (!strcmp(_clname, "NoteContent"))
        return static_cast< NoteContent*>(const_cast< LinkContent*>(this));
    return QObject::qt_metacast(_clname);
}

int LinkContent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
static const uint qt_meta_data_CrossReferenceContent[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_CrossReferenceContent[] = {
    "CrossReferenceContent\0"
};

void CrossReferenceContent::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData CrossReferenceContent::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CrossReferenceContent::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CrossReferenceContent,
      qt_meta_data_CrossReferenceContent, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CrossReferenceContent::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CrossReferenceContent::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CrossReferenceContent::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CrossReferenceContent))
        return static_cast<void*>(const_cast< CrossReferenceContent*>(this));
    if (!strcmp(_clname, "NoteContent"))
        return static_cast< NoteContent*>(const_cast< CrossReferenceContent*>(this));
    return QObject::qt_metacast(_clname);
}

int CrossReferenceContent::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE

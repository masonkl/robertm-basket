/****************************************************************************
** Meta object code from reading C++ file 'basketstatusbar.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/basketstatusbar.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'basketstatusbar.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_BasketStatusBar[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      22,   17,   16,   16, 0x0a,
      48,   16,   16,   16, 0x0a,
      75,   70,   16,   16, 0x0a,
     107,  105,   16,   16, 0x0a,
     144,  135,   16,   16, 0x0a,
     164,   16,   16,   16, 0x0a,
     191,  181,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_BasketStatusBar[] = {
    "BasketStatusBar\0\0hint\0setStatusBarHint(QString)\0"
    "updateStatusBarHint()\0text\0"
    "postStatusbarMessage(QString)\0s\0"
    "setSelectionStatus(QString)\0isLocked\0"
    "setLockStatus(bool)\0setupStatusBar()\0"
    "isUnsaved\0setUnsavedStatus(bool)\0"
};

void BasketStatusBar::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        BasketStatusBar *_t = static_cast<BasketStatusBar *>(_o);
        switch (_id) {
        case 0: _t->setStatusBarHint((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->updateStatusBarHint(); break;
        case 2: _t->postStatusbarMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->setSelectionStatus((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->setLockStatus((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: _t->setupStatusBar(); break;
        case 6: _t->setUnsavedStatus((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData BasketStatusBar::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject BasketStatusBar::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_BasketStatusBar,
      qt_meta_data_BasketStatusBar, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &BasketStatusBar::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *BasketStatusBar::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *BasketStatusBar::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_BasketStatusBar))
        return static_cast<void*>(const_cast< BasketStatusBar*>(this));
    return QObject::qt_metacast(_clname);
}

int BasketStatusBar::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE

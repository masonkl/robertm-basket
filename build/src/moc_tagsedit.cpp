/****************************************************************************
** Meta object code from reading C++ file 'tagsedit.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/tagsedit.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tagsedit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_TagListView[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,
      29,   12,   12,   12, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_TagListView[] = {
    "TagListView\0\0deletePressed()\0"
    "doubleClickedItem()\0"
};

void TagListView::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TagListView *_t = static_cast<TagListView *>(_o);
        switch (_id) {
        case 0: _t->deletePressed(); break;
        case 1: _t->doubleClickedItem(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TagListView::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TagListView::staticMetaObject = {
    { &QTreeWidget::staticMetaObject, qt_meta_stringdata_TagListView,
      qt_meta_data_TagListView, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TagListView::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TagListView::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TagListView::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TagListView))
        return static_cast<void*>(const_cast< TagListView*>(this));
    return QTreeWidget::qt_metacast(_clname);
}

int TagListView::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTreeWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    }
    return _id;
}

// SIGNAL 0
void TagListView::deletePressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void TagListView::doubleClickedItem()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
static const uint qt_meta_data_TagsEditDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      19,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x08,
      25,   15,   15,   15, 0x08,
      36,   15,   15,   15, 0x08,
      45,   15,   15,   15, 0x08,
      56,   15,   15,   15, 0x08,
      68,   15,   15,   15, 0x08,
      88,   79,   15,   15, 0x08,
     116,   15,   15,   15, 0x08,
     133,   15,   15,   15, 0x08,
     148,   15,   15,   15, 0x08,
     169,  159,   15,   15, 0x08,
     228,  223,   15,   15, 0x28,
     265,   15,   15,   15, 0x08,
     278,   15,   15,   15, 0x08,
     287,   15,   15,   15, 0x08,
     298,   15,   15,   15, 0x08,
     311,   15,   15,   15, 0x08,
     324,   15,   15,   15, 0x08,
     338,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_TagsEditDialog[] = {
    "TagsEditDialog\0\0newTag()\0newState()\0"
    "moveUp()\0moveDown()\0deleteTag()\0"
    "renameIt()\0shortcut\0capturedShortcut(KShortcut)\0"
    "removeShortcut()\0removeEmblem()\0"
    "modified()\0item,next\0"
    "currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)\0"
    "item\0currentItemChanged(QTreeWidgetItem*)\0"
    "slotCancel()\0slotOk()\0selectUp()\0"
    "selectDown()\0selectLeft()\0selectRight()\0"
    "resetTreeSizeHint()\0"
};

void TagsEditDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        TagsEditDialog *_t = static_cast<TagsEditDialog *>(_o);
        switch (_id) {
        case 0: _t->newTag(); break;
        case 1: _t->newState(); break;
        case 2: _t->moveUp(); break;
        case 3: _t->moveDown(); break;
        case 4: _t->deleteTag(); break;
        case 5: _t->renameIt(); break;
        case 6: _t->capturedShortcut((*reinterpret_cast< const KShortcut(*)>(_a[1]))); break;
        case 7: _t->removeShortcut(); break;
        case 8: _t->removeEmblem(); break;
        case 9: _t->modified(); break;
        case 10: _t->currentItemChanged((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1])),(*reinterpret_cast< QTreeWidgetItem*(*)>(_a[2]))); break;
        case 11: _t->currentItemChanged((*reinterpret_cast< QTreeWidgetItem*(*)>(_a[1]))); break;
        case 12: _t->slotCancel(); break;
        case 13: _t->slotOk(); break;
        case 14: _t->selectUp(); break;
        case 15: _t->selectDown(); break;
        case 16: _t->selectLeft(); break;
        case 17: _t->selectRight(); break;
        case 18: _t->resetTreeSizeHint(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData TagsEditDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TagsEditDialog::staticMetaObject = {
    { &KDialog::staticMetaObject, qt_meta_stringdata_TagsEditDialog,
      qt_meta_data_TagsEditDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TagsEditDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TagsEditDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TagsEditDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TagsEditDialog))
        return static_cast<void*>(const_cast< TagsEditDialog*>(this));
    return KDialog::qt_metacast(_clname);
}

int TagsEditDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 19)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 19;
    }
    return _id;
}
static const uint qt_meta_data_TagListDelegate[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_TagListDelegate[] = {
    "TagListDelegate\0"
};

void TagListDelegate::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData TagListDelegate::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject TagListDelegate::staticMetaObject = {
    { &QItemDelegate::staticMetaObject, qt_meta_stringdata_TagListDelegate,
      qt_meta_data_TagListDelegate, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &TagListDelegate::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *TagListDelegate::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *TagListDelegate::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TagListDelegate))
        return static_cast<void*>(const_cast< TagListDelegate*>(this));
    return QItemDelegate::qt_metacast(_clname);
}

int TagListDelegate::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QItemDelegate::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE

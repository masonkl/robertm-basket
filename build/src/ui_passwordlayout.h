#include <kdialog.h>
#include <klocale.h>

/********************************************************************************
** Form generated from reading UI file 'passwordlayout.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PASSWORDLAYOUT_H
#define UI_PASSWORDLAYOUT_H

#include <Qt3Support/Q3MimeSourceFactory>
#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QRadioButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "kcombobox.h"

QT_BEGIN_NAMESPACE

class Ui_PasswordLayout
{
public:
    QVBoxLayout *vboxLayout;
    QGroupBox *buttonGroup;
    QVBoxLayout *vboxLayout1;
    QRadioButton *noPasswordRadioButton;
    QRadioButton *passwordRadioButton;
    QHBoxLayout *hboxLayout;
    QRadioButton *publicPrivateRadioButton;
    KComboBox *keyCombo;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *PasswordLayout)
    {
        if (PasswordLayout->objectName().isEmpty())
            PasswordLayout->setObjectName(QString::fromUtf8("PasswordLayout"));
        PasswordLayout->resize(377, 103);
        vboxLayout = new QVBoxLayout(PasswordLayout);
        vboxLayout->setSpacing(KDialog::spacingHint());
        vboxLayout->setContentsMargins(0, 0, 0, 0);
        vboxLayout->setObjectName(QString::fromUtf8("vboxLayout"));
        buttonGroup = new QGroupBox(PasswordLayout);
        buttonGroup->setObjectName(QString::fromUtf8("buttonGroup"));
        vboxLayout1 = new QVBoxLayout(buttonGroup);
        vboxLayout1->setSpacing(KDialog::spacingHint());
        vboxLayout1->setContentsMargins(0, 0, 0, 0);
        vboxLayout1->setObjectName(QString::fromUtf8("vboxLayout1"));
        noPasswordRadioButton = new QRadioButton(buttonGroup);
        noPasswordRadioButton->setObjectName(QString::fromUtf8("noPasswordRadioButton"));

        vboxLayout1->addWidget(noPasswordRadioButton);

        passwordRadioButton = new QRadioButton(buttonGroup);
        passwordRadioButton->setObjectName(QString::fromUtf8("passwordRadioButton"));

        vboxLayout1->addWidget(passwordRadioButton);

        hboxLayout = new QHBoxLayout();
        hboxLayout->setSpacing(KDialog::spacingHint());
        hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
        publicPrivateRadioButton = new QRadioButton(buttonGroup);
        publicPrivateRadioButton->setObjectName(QString::fromUtf8("publicPrivateRadioButton"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(publicPrivateRadioButton->sizePolicy().hasHeightForWidth());
        publicPrivateRadioButton->setSizePolicy(sizePolicy);

        hboxLayout->addWidget(publicPrivateRadioButton);

        keyCombo = new KComboBox(buttonGroup);
        keyCombo->setObjectName(QString::fromUtf8("keyCombo"));

        hboxLayout->addWidget(keyCombo);


        vboxLayout1->addLayout(hboxLayout);


        vboxLayout->addWidget(buttonGroup);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        vboxLayout->addItem(verticalSpacer);


        retranslateUi(PasswordLayout);

        QMetaObject::connectSlotsByName(PasswordLayout);
    } // setupUi

    void retranslateUi(QWidget *PasswordLayout)
    {
        PasswordLayout->setWindowTitle(tr2i18n("Password Protection", 0));
        buttonGroup->setTitle(QString());
        noPasswordRadioButton->setText(tr2i18n("&No protection", 0));
        passwordRadioButton->setText(tr2i18n("Protect basket with a &password", 0));
        publicPrivateRadioButton->setText(tr2i18n("Protect basket with private &key:", 0));
    } // retranslateUi

};

namespace Ui {
    class PasswordLayout: public Ui_PasswordLayout {};
} // namespace Ui

QT_END_NAMESPACE

#endif // PASSWORDLAYOUT_H


/****************************************************************************
** Meta object code from reading C++ file 'kcolorcombo2.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/kcolorcombo2.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'kcolorcombo2.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_KColorCombo2[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       2,   34, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      23,   14,   13,   13, 0x05,

 // slots: signature, parameters, type, tag, flags
      45,   39,   13,   13, 0x0a,
      62,   39,   13,   13, 0x0a,
      86,   13,   13,   13, 0x08,

 // properties: name, type, flags
      39,  100, 0x43095103,
     107,  100, 0x43095103,

       0        // eod
};

static const char qt_meta_stringdata_KColorCombo2[] = {
    "KColorCombo2\0\0newColor\0changed(QColor)\0"
    "color\0setColor(QColor)\0setDefaultColor(QColor)\0"
    "popupClosed()\0QColor\0defaultColor\0"
};

void KColorCombo2::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KColorCombo2 *_t = static_cast<KColorCombo2 *>(_o);
        switch (_id) {
        case 0: _t->changed((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 1: _t->setColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 2: _t->setDefaultColor((*reinterpret_cast< const QColor(*)>(_a[1]))); break;
        case 3: _t->popupClosed(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData KColorCombo2::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KColorCombo2::staticMetaObject = {
    { &KComboBox::staticMetaObject, qt_meta_stringdata_KColorCombo2,
      qt_meta_data_KColorCombo2, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KColorCombo2::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KColorCombo2::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KColorCombo2::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KColorCombo2))
        return static_cast<void*>(const_cast< KColorCombo2*>(this));
    return KComboBox::qt_metacast(_clname);
}

int KColorCombo2::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = KComboBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QColor*>(_v) = color(); break;
        case 1: *reinterpret_cast< QColor*>(_v) = defaultColor(); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setColor(*reinterpret_cast< QColor*>(_v)); break;
        case 1: setDefaultColor(*reinterpret_cast< QColor*>(_v)); break;
        }
        _id -= 2;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 2;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 2;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void KColorCombo2::changed(const QColor & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
static const uint qt_meta_data_KColorPopup[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_KColorPopup[] = {
    "KColorPopup\0\0closed()\0"
};

void KColorPopup::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        KColorPopup *_t = static_cast<KColorPopup *>(_o);
        switch (_id) {
        case 0: _t->closed(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData KColorPopup::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject KColorPopup::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_KColorPopup,
      qt_meta_data_KColorPopup, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &KColorPopup::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *KColorPopup::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *KColorPopup::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_KColorPopup))
        return static_cast<void*>(const_cast< KColorPopup*>(this));
    return QWidget::qt_metacast(_clname);
}

int KColorPopup::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 1)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void KColorPopup::closed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE

# CMake generated Testfile for 
# Source directory: /home/masonkl/CMSC461/SemesterProject/robertm-basket
# Build directory: /home/masonkl/CMSC461/SemesterProject/robertm-basket/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(src)
subdirs(po)
subdirs(tags)
subdirs(welcome)
subdirs(backgrounds)
subdirs(images)
subdirs(file-integration)

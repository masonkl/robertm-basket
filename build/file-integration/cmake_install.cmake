# Install script for directory: /home/masonkl/CMSC461/SemesterProject/robertm-basket/file-integration

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "RelWithDebInfo")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(EXISTS "$ENV{DESTDIR}/usr/local/lib/kde4/basketthumbcreator.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/kde4/basketthumbcreator.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/lib/kde4/basketthumbcreator.so"
         RPATH "/usr/local/lib")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/lib/kde4/basketthumbcreator.so")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/lib/kde4" TYPE MODULE FILES "/home/masonkl/CMSC461/SemesterProject/robertm-basket/build/lib/basketthumbcreator.so")
  if(EXISTS "$ENV{DESTDIR}/usr/local/lib/kde4/basketthumbcreator.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/lib/kde4/basketthumbcreator.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}/usr/local/lib/kde4/basketthumbcreator.so"
         OLD_RPATH "::::::::::::::"
         NEW_RPATH "/usr/local/lib")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/lib/kde4/basketthumbcreator.so")
    endif()
  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/kde4/services/basketthumbcreator.desktop")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/kde4/services" TYPE FILE FILES "/home/masonkl/CMSC461/SemesterProject/robertm-basket/file-integration/basketthumbcreator.desktop")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  
execute_process(COMMAND /usr/bin/xdg-mime install --novendor
  /home/masonkl/CMSC461/SemesterProject/robertm-basket/file-integration/basket.xml)
execute_process(COMMAND /usr/bin/xdg-desktop-menu install
  --novendor /home/masonkl/CMSC461/SemesterProject/robertm-basket/src/basket.desktop)
execute_process(COMMAND /usr/bin/xdg-mime default
  /home/masonkl/CMSC461/SemesterProject/robertm-basket/src/basket.desktop application/x-basket-item)

endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  
    set(DESTDIR_VALUE "$ENV{DESTDIR}")
    if (NOT DESTDIR_VALUE)
        file(WRITE "/usr/local/share/icons/hicolor/temp.txt" "update")
        file(REMOVE "/usr/local/share/icons/hicolor/temp.txt")
    endif (NOT DESTDIR_VALUE)
    
endif()

